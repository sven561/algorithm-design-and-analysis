﻿using System;
using System.Drawing;

namespace 算法设计与分析.TSP
{
    public class TravelingSalesmanProblem
    {
        private Point[] _points;
        private double[,] _distanceMatrix;
        private double _initialTemperature = 0d;
        private double _currentTemperature = 0d;
        private int _routerCount;
        private int[] _currentRouter;
        private int[] _resultRouter;
        private Random _rdm;
        private bool _IsCalculated = false;
        private DateTime calculate_begin;
        private DateTime calculate_end;

        public TravelingSalesmanProblem(Point[] pts)
        {
            _points = (Point[])pts.Clone();

            _routerCount = _points.Length;
            _currentRouter = new int[_routerCount];
            _resultRouter = new int[_routerCount];
            _distanceMatrix = new double[_routerCount, _routerCount];

            _rdm = new Random((int)DateTime.Now.Ticks);

            double minDistance = 10000d;
            double maxDistance = 0d;

            for (int i = 0; i < _points.Length; i++)
            {
                _currentRouter[i] = i; // init point router 1 > 2 > 3  > n
                _resultRouter[i] = i;
                for (int j = i + 1; j < _points.Length; j++)
                {
                    double offsetX = (double)(_points[i].X - _points[j].X);
                    double offsetY = (double)(_points[i].Y - _points[j].Y);
                    double distance = Math.Sqrt(offsetX * offsetX + offsetY * offsetY); //计算两点间的距离
                    _distanceMatrix[i, j] = distance;
                    _distanceMatrix[j, i] = distance;
                    if (distance > maxDistance) maxDistance = distance;
                    if (distance < minDistance) minDistance = distance;
                }
            }
            _initialTemperature = 20 * (_routerCount * maxDistance - _routerCount * minDistance);// 计算起始温度
            _currentTemperature = _initialTemperature;
        }

        // 得出最优距离
        public double GetTotalDistancOptimization()
        {
            if (!_IsCalculated)
                Calculation();
            return CalculateTotalDistance(_resultRouter);
        }

        // 得到最优路由
        public int[] GetRouterOptimization()
        {
            if (!_IsCalculated)
                Calculation();
            return _resultRouter;
        }

        public double GetCalculationalTime()
        {
            if (_IsCalculated)
            {
                TimeSpan span = calculate_end - calculate_begin;
                return span.TotalMilliseconds;
            }
            else
                return 0d;
        }

        // 求出最优解
        private void Calculation()
        {
            //Random rdm = new Random((int)DateTime.Now.Ticks);
            calculate_begin = DateTime.Now;
            while (true)
            {
                int IterCount = 0;
                while (true)
                {
                    double totalDistance1 = CalculateTotalDistance(_resultRouter);
                    BuildRandomRouter(); // change router
                    double totalDistance2 = CalculateTotalDistance(_currentRouter);

                    double deltaTotalDistance = totalDistance2 - totalDistance1;//delta
                                                                                //原理见顶部注释
                    if (deltaTotalDistance <= 0d)
                    {
                        for (int i = 0; i < _resultRouter.Length; i++)
                            _resultRouter[i] = _currentRouter[i];//赋值到结果路由
                    }
                    else
                    {
                        //原理见顶部注释
                        double probability = Math.Exp(-(deltaTotalDistance / _currentTemperature));
                        double lambda = ((double)(_rdm.Next() % 10000)) / 10000d;//随机指定的正数lambda
                        if (probability > lambda)
                        {
                            for (int i = 0; i < _resultRouter.Length; i++)
                                _resultRouter[i] = _currentRouter[i];
                        }
                    }
                    if (IterCount >= _routerCount * _routerCount * _routerCount) //某一温度下的循环次数是点个数的三次方
                    {
                        break;
                    }
                    else
                    {
                        IterCount++;
                    }
                }
                if (_currentTemperature <= 0.01) //温度小于等于0.01时跳出外循环
                {
                    break;
                }
                else
                {
                    _currentTemperature = 0.96 * _currentTemperature;// 温度下降方式 T（k+1） = K*T（k），K=0.95
                }
            }
            _IsCalculated = true;
            calculate_end = DateTime.Now;
        }

        private double CalculateTotalDistance(int[] router)
        {
            double totalDistance = 0d;
            for (int i = 0; i < router.Length - 1; i++)
            {
                totalDistance += _distanceMatrix[router[i], router[i + 1]];
            }
            totalDistance += _distanceMatrix[_currentRouter[router.Length - 1], router[0]];
            return totalDistance;
        }

        // 按某种概率过程产生新的搜索状态
        // 随机产生一个新的路由
        private void BuildRandomRouter()
        {
            int rdm1, rdm2;
            while (true)
            {
                rdm1 = _rdm.Next() % _currentRouter.Length - 1;
                if (rdm1 >= 0) break;
            }
            while (true)
            {
                rdm2 = _rdm.Next() % _currentRouter.Length - 1;
                if (rdm1 != rdm2 && rdm2 >= 0)
                {
                    int temp = _currentRouter[rdm1];
                    _currentRouter[rdm1] = _currentRouter[rdm2];
                    _currentRouter[rdm2] = temp;
                    break;
                }
            }
        }
    }
}