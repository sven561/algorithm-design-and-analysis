﻿using System;
using System.Collections.Generic;
using System.Drawing;
using 算法设计与分析.TSP;
using 算法设计与分析.动态规划;

namespace 算法设计与分析
{
    public class Progress
    {
        //public static void Main()
        //{
        //    #region SortingProblem
        //    //const int len = 100;
        //    //var bytes = new int[len];
        //    //var random = new Random();
        //    //for (int i = 0; i < len; i++)
        //    //{
        //    //    bytes[i] = random.Next(len);
        //    //}

        //    ////var value = SelectionProblem.Selection(bytes, 0, len - 1, 8);
        //    //var value = SelectionProblem.RandomizedSelection(bytes, 0, len - 1, 8);
        //    //Console.WriteLine($"第8号位置为{value}");

        //    ////SortingProblem.MergeSort(bytes, 0, len - 1);
        //    ////SortingProblem.QuickSort(bytes, 0, len - 1);
        //    //SortingProblem.RandomizedQuickSort(bytes, 0, len - 1);
        //    //for (int i = 0; i < len; i++)
        //    //{
        //    //    Console.WriteLine(bytes[i]);
        //    //}

        //    #endregion

        //    #region MaxSubArray

        //    //const int len = 10;
        //    //var bytes = new int[len];
        //    //var random = new Random();
        //    //for (int i = 0; i < len; i++)
        //    //{
        //    //    bytes[i] = random.Next(-len, len);
        //    //}

        //    //var sum = MaxContinuousSubArray.MaxSubArray(bytes, 0, len - 1);
        //    //var str = string.Empty;
        //    //for (int i = 0; i < len; i++)
        //    //{
        //    //    str += $"{bytes[i]},";
        //    //}
        //    //Console.WriteLine($"{str}");
        //    //Console.WriteLine(sum);

        //    //var bytes = new[] { 1, -2, 4, 5, -2, 8, 3, -2, 6, 3, 7, -1 };
        //    //int[] D, rec;
        //    //MaxContinuousSubArray.MaxContinuousSubarrayDP(bytes, out D, out rec);

        //    //int SMax, l, r;
        //    //MaxContinuousSubArray.GetSMaxAndLR(D, D.Length, rec, out SMax, out l, out r);
        //    //Console.WriteLine($"最优{SMax},左下标{l},右下标{r}");


        //    #endregion

        //    #region CountingInversions

        //    //var bytes = new int[] {13, 8, 10, 6, 15, 18, 12, 20, 9, 14, 17, 19};
        //    //var sum = CountingInversions.CountInver(bytes, 0, 11);
        //    //Console.WriteLine($"{sum}个逆序对");

        //    #endregion

        //    #region Knapsack01Problem

        //    //var p = Knapsack01Problem.KnapsackSR2(4, 13);
        //    //Console.WriteLine(p);

        //    //Knapsack01Problem.KnapsackDP(13);
        //    //Knapsack01Problem.AnalyseRecord(13);

        //    #endregion

        //    #region LongestCommonSubsequenceProblem

        //    //var X = new[] { "A", "B", "C", "B", "D", "A", "B" };
        //    //var Y = new[] { "B", "D", "C", "A", "B", "A" };

        //    //int[,] c;
        //    //string[,] rec;
        //    //LongestCommonSubsequenceProblem.LongestCommonSubsequence(X, Y, out c, out rec);
        //    //LongestCommonSubsequenceProblem.PrintLCS(rec, X, X.Length, Y.Length);

        //    #endregion

        //    #region LongestCommonSubstringProblem

        //    //var X = new[] { "A", "B", "C", "A", "D", "B", "B" };
        //    //var Y = new[] { "B", "C", "E", "D", "B", "B" };

        //    //int[,] c;
        //    //string[,] rec;
        //    //int lMax, pMax;
        //    //LongestCommonSubstringProblem.LongestCommonSubstring(X, Y, out c, out lMax, out pMax);
        //    //LongestCommonSubstringProblem.PrintLCS(X, lMax, pMax);

        //    #endregion

        //    #region LongestCommonSubstringProblem

        //    //var X = new[] { "A", "B", "C", "B", "D", "A", "B" };
        //    //var Y = new[] { "B", "D", "C", "A", "B", "A" };

        //    //int[,] c;
        //    //string[,] rec;
        //    //MinimumEditDistance.Process(X, Y, out c, out rec);
        //    //MinimumEditDistance.PrintMED(rec, X, Y, X.Length, Y.Length);

        //    #endregion

        //    #region RodCuttingProblem

        //    // var p = new[] { 1, 5, 8, 9, 10, 17, 17, 20, 24, 24 };
        //    // int[] c, rec;
        //    // RodCuttingProblem.Process(p, out c, out rec);

        //    #endregion

        //    #region TravelingSalesmanProblem

        //    //const int N = 31;
        //    //var citys = new int[N, 2]
        //    //{
        //    //{1304,2312},{3639,1315},{4177,2244},{3712,1399},{3488,1535},{3326,1556},{3238,1229},{4196,1004},{4312,790},
        //    //{4386,570},{3007,1970},{2562,1756},{2788,1491},{2381,1676},{1332,695},{3715,1678},{3918,2179},{4061,2370},{3780,2212},{3676,2578},{4029,2838},
        //    //{4263,2931},{3429,1908},{3507,2367},{3394,2643},{3439,3201},{2935,3240},{3140,3550},{2545,2357},{2778,2826},{2370,2975}
        //    //};// 中国31个城市坐标

        //    //var list = new List<Point>();
        //    //for (int i = 0; i < N; i++)
        //    //{
        //    //    var x = citys[i, 0];
        //    //    var y = citys[i, 1];
        //    //    list.Add(new Point(x, y));
        //    //}

        //    //var p = new TravelingSalesmanProblem(list.ToArray());
        //    //var path = p.GetRouterOptimization();
        //    //foreach (var i in path)
        //    //{
        //    //    Console.WriteLine($"经过城市序号{i},坐标为{list[i].X}-{list[i].Y}");
        //    //}

        //    //Console.WriteLine(p.GetCalculationalTime());
        //    //Console.WriteLine(p.GetTotalDistancOptimization());

        //    #endregion

        //    Console.ReadKey();
        //}
    }
}