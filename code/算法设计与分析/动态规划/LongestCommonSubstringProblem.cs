﻿using System;

namespace 算法设计与分析.动态规划
{
    public class LongestCommonSubstringProblem
    {
        public static void LongestCommonSubstring(string[] X, string[] Y, out int[,] c, out int lMax, out int pMax)
        {
            var n = X.Length;
            var m = Y.Length;

            c = new int[n + 1, m + 1];
            lMax = 0;
            pMax = 0;

            for (int i = 1; i < n + 1; i++)
            {
                for (int j = 1; j < m + 1; j++)
                {
                    if (X[i - 1] != Y[j - 1])
                    {
                        c[i, j] = 0;
                    }
                    else
                    {
                        c[i, j] = c[i - 1, j - 1] + 1;
                        if (c[i, j] > lMax)
                        {
                            lMax = c[i, j];
                            pMax = i;
                        }
                    }
                }
            }
        }

        public static void PrintLCS(string[] X, int lMax, int pMax)
        {
            if (lMax == 0) return;
            for (int i = pMax - lMax + 1; i <= pMax; i++)
            {
                Console.WriteLine(X[i - 1]);
            }
        }
    }
}