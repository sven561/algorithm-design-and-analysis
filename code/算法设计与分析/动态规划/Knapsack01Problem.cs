﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace 算法设计与分析.动态规划
{
    public class Knapsack01Problem
    {
        public static List<Product> Products { get; set; } = new List<Product>
        {
            new Product {P = 24, V = 10},
            new Product {P = 2, V = 3},
            new Product {P = 9, V = 4},
            new Product {P = 10, V = 5},
            new Product {P = 9, V = 4},
        };

        public static int[,] P;
        public static int[,] rec;

        public static int KnapsackSR(int left, int right, int c)
        {
            if (c < 0) return int.MinValue;
            if (right <= left - 1) return 0;
            var p1 = KnapsackSR(left, right - 1, c - Products[right].V);
            var p2 = KnapsackSR(left, right - 1, c);
            return Math.Max(p1, p2);
        }

        public static int KnapsackSR2(int i, int c)
        {
            if (c < 0) return int.MinValue;
            if (i <= 0) return 0;
            var p1 = KnapsackSR2(i - 1, c - Products[i].V);
            var p2 = KnapsackSR2(i - 1, c);
            return Math.Max(p1 + Products[i].P, p2);
        }

        public static void KnapsackDP(int c)
        {
            KnapsackDP(Products.Count, Products.Select(x => x.P).ToArray(), Products.Select(x => x.V).ToArray(), c);
        }

        public static void KnapsackDP(int n, int[] p, int[] v, int c)
        {
            P = new int[n + 1, c + 1];
            rec = new int[n + 1, c + 1];
            //P[i,j]:前i个商品可选，背包容量为j时的最大总价格
            for (int i = 1; i < n + 1; i++)//计算P的行，i-1代表物品序号，使用v[i-1]和p[i-1]
            {
                for (int j = 1; j < c + 1; j++)//计算P的列，j代表重量
                {
                    //可以放入物品，而且放入后比上一个方案重
                    if ((v[i - 1] <= j) && (p[i - 1] + P[i - 1, j - v[i - 1]] > P[i - 1, j]))
                    {
                        P[i, j] = p[i - 1] + P[i - 1, j - v[i - 1]];//放入物品
                        rec[i, j] = 1;//记录
                    }
                    else//保留上一个方案
                    {
                        P[i, j] = P[i - 1, j];
                    }
                }
            }
        }

        public static void AnalyseRecord(int c)
        {
            var k = c;
            for (int i = Products.Count; i >= 1; i--)
            {
                if (Knapsack01Problem.rec[i, k] == 1)
                {
                    Console.WriteLine($"选择商品{i}");
                    k = k - Knapsack01Problem.Products[i - 1].V;
                }
                else
                {
                    Console.WriteLine($"不选择商品{i}");
                }
            }
        }
    }

    /// <summary>
    /// 商品
    /// </summary>
    public class Product
    {
        /// <summary>
        /// 体积
        /// </summary>
        public int V { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        public int P { get; set; }
    }
}