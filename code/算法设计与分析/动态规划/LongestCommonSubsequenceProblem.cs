﻿using System;

namespace 算法设计与分析.动态规划
{
    public class LongestCommonSubsequenceProblem
    {
        public static void LongestCommonSubsequence(string[] X, string[] Y, out int[,] c, out string[,] rec)
        {
            var n = X.Length;
            var m = Y.Length;

            c = new int[n + 1, m + 1];
            rec = new string[n + 1, m + 1];
            //for (int i = 0; i < n + 1; i++)
            //{
            //    for (int j = 0; j < m + 1; j++)
            //    {
            //        rec[i, j] = string.Empty;
            //    }
            //}

            for (int i = 1; i < n + 1; i++)
            {
                for (int j = 1; j < m + 1; j++)
                {
                    if (X[i - 1] == Y[j - 1])
                    {
                        c[i, j] = c[i - 1, j - 1] + 1;
                        rec[i, j] = "LU";
                    }
                    else if (c[i - 1, j] >= c[i, j - 1])
                    {
                        c[i, j] = c[i - 1, j];
                        rec[i, j] = "U";
                    }
                    else
                    {
                        c[i, j] = c[i, j - 1];
                        rec[i, j] = "L";
                    }
                }
            }
        }

        public static void PrintLCS(string[,] rec, string[] X, int i, int j)
        {
            if (i == 0 || j == 0) return;
            ;
            if (rec[i, j] == "LU")
            {
                PrintLCS(rec, X, i - 1, j - 1);
                Console.WriteLine(X[i - 1]);
            }
            else if (rec[i, j] == "U")
            {
                PrintLCS(rec, X, i - 1, j);
            }
            else
            {
                PrintLCS(rec, X, i, j - 1);
            }
        }
    }
}