﻿using System;

namespace 算法设计与分析.动态规划
{
    public class RodCuttingProblem
    {
        public static void Process(int[] p, out int[] c, out int[] rec)
        {
            var n = p.Length;
            c = new int[n + 1];
            rec = new int[n + 1];
            for (int j = 1; j < n + 1; j++)
            {
                var q = p[j - 1];
                rec[j] = j;
                for (int i = 1; i < j; i++)
                {
                    if (q < p[i - 1] + c[j - i])
                    {
                        q = p[i - 1] + c[j - i];
                        rec[j] = i;
                    }
                }

                c[j] = q;
            }

            while (n > 0)
            {
                Console.WriteLine(rec[n]);
                n = n - rec[n];
            }
        }
    }
}