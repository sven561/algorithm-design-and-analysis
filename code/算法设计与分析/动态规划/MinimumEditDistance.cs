﻿using System;

namespace 算法设计与分析.动态规划
{
    public class MinimumEditDistance
    {
        public static void Process(string[] s, string[] t, out int[,] d, out string[,] rec)
        {
            var n = s.Length;
            var m = t.Length;

            d = new int[n + 1, m + 1];
            rec = new string[n + 1, m + 1];

            for (int i = 0; i < n + 1; i++)
            {
                d[i, 0] = i;
                rec[i, 0] = "U";
            }

            for (int j = 0; j < m + 1; j++)
            {
                d[0, j] = j;
                rec[0, j] = "L";
            }

            for (int i = 1; i < n + 1; i++)
            {
                for (int j = 1; j < m + 1; j++)
                {
                    var c = s[i - 1] != t[j - 1] ? 1 : 0;
                    var replace = d[i - 1, j - 1] + c;
                    var delete = d[i - 1, j] + 1;
                    var insert = d[i, j - 1] + 1;
                    if (replace <= insert && replace <= delete)
                    {
                        d[i, j] = d[i - 1, j - 1] + c;
                        rec[i, j] = "LU";
                    }
                    else if (insert <= replace && insert <= delete)
                    {
                        d[i, j] = d[i, j - 1] + 1;
                        rec[i, j] = "L";
                    }
                    else
                    {
                        d[i, j] = d[i - 1, j] + 1;
                        rec[i, j] = "U";
                    }
                }
            }
        }

        public static void PrintMED(string[,] rec, string[] s, string[] t, int i, int j)
        {
            if (i == 0 && j == 0) return;

            if (rec[i, j] == "LU")
            {
                PrintMED(rec, s, t, i - 1, j - 1);
                if (s[i - 1] == t[j - 1])
                {
                    Console.WriteLine($"无需操作");
                }
                else
                {
                    Console.WriteLine($"用{t[j - 1]}替换{s[i - 1]}");
                }
            }
            else if (rec[i, j] == "U")
            {
                PrintMED(rec, s, t, i - 1, j);
                Console.WriteLine($"删除{s[i - 1]}");
            }
            else
            {
                PrintMED(rec, s, t, i, j - 1);
                Console.WriteLine($"插入{t[j - 1]}");
            }
        }
    }
}