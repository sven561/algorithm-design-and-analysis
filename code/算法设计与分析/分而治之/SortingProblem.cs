﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 算法设计与分析
{
    /// <summary>
    /// 数组归并排序问题
    /// </summary>
    public class SortingProblem
    {
        public static void MergeSort(int[] A, int left, int right)
        {
            if (left >= right) return;    //数组长度为1时
            var sum = left + right;
            var mid = sum >> 1;
            MergeSort(A, left, mid);
            MergeSort(A, mid + 1, right);
            Merge(A, left, mid, right);
        }

        private static void Merge(int[] ints, int left, int mid, int right)
        {
            var copy = new int[ints.Length];
            Array.Copy(ints, left, copy, left, right - left + 1);

            var i = left;
            var j = mid + 1;
            var k = 0;

            while (i <= mid && j <= right)
            {
                if (copy[i] <= copy[j])
                    ints[left + k++] = copy[i++];
                else
                    ints[left + k++] = copy[j++];
            }

            while (i <= mid)
            {
                ints[left + k++] = copy[i++];
            }

            while (j <= right)
            {
                ints[left + k++] = copy[j++];
            }
        }

        public static void QuickSort(int[] A, int left, int right)
        {
            if (left >= right) return;
            var index = Partition(A, left, right);
            QuickSort(A, left, index - 1);
            QuickSort(A, index + 1, right);
        }

        public static int Partition(int[] A, int left, int right)
        {
            var x = A[right];
            var i = left - 1;
            for (int j = left; j < right; j++)
            {
                if (A[j] <= x)
                {
                    Exchange(A, i + 1, j);
                    i++;
                }
            }

            Exchange(A, i + 1, right);
            return i + 1;
        }

        private static void Exchange(int[] A, int i, int j)
        {
            var tmp = A[i];
            A[i] = A[j];
            A[j] = tmp;
        }

        public static void RandomizedQuickSort(int[] A, int left, int right)
        {
            if (left >= right) return;
            var index = RandomizedPartition(A, left, right);
            RandomizedQuickSort(A, left, index - 1);
            RandomizedQuickSort(A, index + 1, right);
        }

        public static int RandomizedPartition(int[] A, int left, int right)
        {
            var s = new Random().Next(left, right);
            Exchange(A, s, right);
            return Partition(A, left, right);
        }
    }
}
