﻿using System;

namespace 算法设计与分析
{
    /// <summary>
    /// 数组的最大子数组问题
    /// </summary>
    public class MaxContinuousSubArray
    {
        public static int MaxSubArray(int[] X, int left, int right)
        {
            if (left >= right) return X[left];
            var sum = left + right;
            var mid = sum >> 1;
            var s1 = MaxSubArray(X, left, mid);
            var s2 = MaxSubArray(X, mid + 1, right);
            var s3 = CrossingSubArray(X, left, mid, right);
            return Math.Max(Math.Max(s1, s2), s3);
        }

        private static int CrossingSubArray(int[] ints, int left, int mid, int right)
        {
            var sLeft = int.MinValue;
            var sum = 0;
            for (int i = mid; i >= left; i--)
            {
                sum += ints[i];
                sLeft = Math.Max(sLeft, sum);
            }

            var sRight = int.MinValue;
            sum = 0;
            for (int i = mid + 1; i <= right; i++)
            {
                sum += ints[i];
                sRight = Math.Max(sRight, sum);
            }

            return sLeft + sRight;
        }

        public static void MaxContinuousSubarrayDP(int[] X, out int[] D, out int[] rec)
        {
            var n = X.Length;
            D = new int[n];
            rec = new int[n];
            var p = n - 1;//最后位置下标
            D[p] = X[p];//D最后位置记录
            rec[p] = p;//rec最后位置记录
            for (int i = n - 2; i >= 0; i--)//从倒数第二位置向前到首个位置
            {
                if (D[i + 1] > 0)
                {
                    D[i] = X[i] + D[i + 1];
                    rec[i] = rec[i + 1];
                }
                else
                {
                    D[i] = X[i];
                    rec[i] = i;
                }
            }
        }

        public static void GetSMaxAndLR(int[] D, int n, int[] rec, out int SMax, out int l, out int r)
        {
            //查找解
            SMax = D[0];
            l = 0;
            r = 0;
            for (int i = 1; i < n; i++)
            {
                if (SMax < D[i])
                {
                    SMax = D[i];
                    l = i;
                    r = rec[i];
                }
            }
        }
    }
}