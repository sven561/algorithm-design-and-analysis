﻿using System;

namespace 算法设计与分析
{
    /// <summary>
    /// 数组的逆序对计数问题
    /// </summary>
    public class CountingInversions
    {
        public static int CountInver(int[] A, int left, int right)
        {
            if (left >= right) return 0;
            var sum = left + right;
            var mid = sum >> 1;
            var s1 = CountInver(A, left, mid);
            var s2 = CountInver(A, mid + 1, right);
            var s3 = MergeCount(A, left, mid, right);
            return s1 + s2 + s3;
        }

        private static int MergeCount(int[] ints, int left, int mid, int right)
        {
            var s3 = 0;
            var copy = new int[ints.Length];
            Array.Copy(ints, left, copy, left, right - left + 1);

            var i = left;
            var j = mid + 1;
            var k = 0;

            while (i <= mid && j <= right)
            {
                if (copy[i] <= copy[j])
                {
                    ints[left + k++] = copy[i++];
                }
                else
                {
                    ints[left + k++] = copy[j++];
                    s3 += mid - i + 1;
                }
            }

            while (i <= mid)
            {
                ints[left + k++] = copy[i++];
            }

            while (j <= right)
            {
                ints[left + k++] = copy[j++];
            }

            return s3;
        }
    }
}