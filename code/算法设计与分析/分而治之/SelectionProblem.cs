﻿using System.Runtime.InteropServices;

namespace 算法设计与分析
{
    public class SelectionProblem
    {
        public static int Selection(int[] A, int left, int right, int k)
        {
            var x = 0;
            var q = SortingProblem.Partition(A, left, right);
            if (k == q - left + 1)
            {
                x = A[q];
            }
            else if (k < q - left + 1)
            {
                x = Selection(A, left, q - 1, k);
            }
            else
            {
                x = Selection(A,q + 1, right, k - (q - left + 1));
            }

            return x;
        }

        public static int RandomizedSelection(int[] A, int left, int right, int k)
        {
            var x = 0;
            var q = SortingProblem.RandomizedPartition(A, left, right);
            if (k == q - left + 1)
            {
                x = A[q];
            }
            else if (k < q - left + 1)
            {
                x = RandomizedSelection(A, left, q - 1, k);
            }
            else
            {
                x = RandomizedSelection(A, q + 1, right, k - (q - left + 1));
            }

            return x;
        }
    }
}