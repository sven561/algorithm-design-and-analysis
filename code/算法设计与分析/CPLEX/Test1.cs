﻿using System;
using ILOG.Concert;
using ILOG.CPLEX;

namespace 算法设计与分析.CPLEX
{
    public class Test1
    {
        public static void Main(string[] args)
        {
            //实例化一个空模型
            Cplex cplexModel = new Cplex();

            //生成决策变量并约束范围
            INumVar[][] deVar = new INumVar[1][]; //交叉数组用于存储决策变量
            double[] lb = {0.0, 0.0, 0.0, 0.0}; //lb(low bound)与ub定义决策变量的上下界
            double[] ub = {double.MaxValue, double.MaxValue, double.MaxValue, double.MaxValue};
            string[] deVarName = {"x11", "x12", "x21", "x22"}; //决策变量名
            INumVar[] x = cplexModel.NumVarArray(4, lb, ub, deVarName); //生成决策变量
            deVar[0] = x;

            //生成目标函数
            double[] objCoef = {50.0, 70.0, 50.0, 70.0}; //目标函数系数(object coefficient)
            cplexModel.AddMinimize(cplexModel.ScalProd(x, objCoef)); //数量相乘（scalar product）

            //生成约束条件
            IRange[][] rng = new IRange[1][]; //存放约束
            rng[0] = new IRange[4];
            //AddLe为<=，AddGe为>=,AddEq为=
            rng[0][0] = cplexModel.AddLe(
                cplexModel.Sum(cplexModel.Prod(1.0, x[3]),
                    cplexModel.Prod(1.0, x[1])), 112.0, "c1");
            rng[0][1] = cplexModel.AddLe(
                cplexModel.Sum(cplexModel.Prod(1.0, x[0]),
                    cplexModel.Prod(1.0, x[2])), 104.0, "c2");
            rng[0][2] = cplexModel.AddEq(
                cplexModel.Sum(cplexModel.Prod(20.0, x[0]),
                    cplexModel.Prod(40.0, x[1])), 3200.0, "c3");
            rng[0][3] = cplexModel.AddEq(
                cplexModel.Sum(cplexModel.Prod(10.0, x[2]),
                    cplexModel.Prod(30.0, x[3])), 2000.0, "c4");

            if (cplexModel.Solve())
            {
                int nvars = cplexModel.GetValues(deVar[0]).Length;
                for (int j = 0; j < nvars; ++j)
                {
                    cplexModel.Output().WriteLine("Variable   " + j + ": Value = " + cplexModel.GetValues(deVar[0])[j]);
                }
            }

            cplexModel.ExportModel("lpex1.lp");
            Console.ReadKey();

        }
    }
}